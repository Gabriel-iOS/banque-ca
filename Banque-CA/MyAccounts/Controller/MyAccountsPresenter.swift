//
//  MyAccountsPresenter.swift
//  Banque-CA
//
//  Created by Gabriel on 22/12/2019.
//  Copyright © 2019 Gabriel. All rights reserved.
//

import Foundation
import UIKit

enum BankSection: Int {
    case creditAgricole = 0
    case other
}

class MyAccountsPresenter {
    
    let view: MyAccountsViewControllerProtocol
    let manager: MyAccountManager
    
    let creditAgricoleBankId = 1
    
    enum BankSectionTitle: String {
        case creditAgricole = "Crédit Agricole"
        case other = "Autres Banques"
    }
    
    var bankSectionArray = [BankSection : [BankData]]()
    var bankArray = [BankData]()
    var dataLoaded = false
    
    var selectedAccount: AccountData?
    
    init(view: MyAccountsViewControllerProtocol, manager: MyAccountManager) {
        self.view = view
        self.manager = manager
    }
}

extension MyAccountsPresenter {
    
    private func sortBankArray() {
        
        bankSectionArray[.creditAgricole] = [BankData]()
        bankSectionArray[.other] = [BankData]()
        
        let isCreditAgricoleBank = { (bankData: BankData) -> Bool in
            return bankData.isCA == self.creditAgricoleBankId
        }
        
        var isCreditAgricoleBankFound = true
        var bankArrayTmp = bankArray
        
        repeat {
            guard let itemIndex = bankArrayTmp.firstIndex(where: isCreditAgricoleBank)
            else {
                isCreditAgricoleBankFound = false
                break
            }
            
            let item = bankArrayTmp.remove(at: itemIndex)
            bankSectionArray[.creditAgricole]?.append(item)
            
        } while isCreditAgricoleBankFound
        
        bankSectionArray[.other]?.append(contentsOf: bankArrayTmp)
        
        bankSectionArray[.creditAgricole]?.sort { $0.title < $1.title }
        bankSectionArray[.other]?.sort { $0.title < $1.title }
    }
}

extension MyAccountsPresenter: MyAccountsPresenterProtocol {
    
    func load() {
        manager.getAllBankAccount { (bankArray, error) in
            
            guard let bankArray = bankArray else {
                if let error = error {
                        print(error)
                }
                return
            }
            
            self.dataLoaded = true
            self.bankArray = bankArray
            self.sortBankArray()
            self.view.reloadData()
        }
    }
    
    func getBankHeader(bankSection: BankSection, section: Int, cell: BankHeaderCell) -> BankHeaderCell {
        if dataLoaded {
            if let bankArray = self.bankSectionArray[bankSection] {
                
                let bank = bankArray[section]
                
                cell.delegate = self
                cell.section = section
                cell.bankSection = bankSection
                cell.titleLabel?.text = bank.title
                cell.amountLabel?.text = "\(bank.amount) €"
                
                cell.setCollapsed(collapsed: bank.isCollapsed)
            }
        }
        
        return cell
    }
    
    func getBankAccountCell(bankSection: BankSection, indexPath: IndexPath, cell: BankAccountCell) -> BankAccountCell {
        if dataLoaded {
            if let bankArray = self.bankSectionArray[bankSection] {
                
                let bank = bankArray[indexPath.section]
                let account = bank.accounts[indexPath.row]
                
                cell.titleLabel?.text = account.title
                cell.amountLabel?.text = "\(account.amount) €"
            }
        }
        
        return cell
    }
    
    func getSectionBankAccountCount(bankSection: BankSection) -> Int {
        if dataLoaded {
            return bankSectionArray[bankSection]?.count ?? 0
        }
        return 0
    }
    
    func getBankAccountCount(bankSection: BankSection, section: Int) -> Int {
        if dataLoaded {
            if let bankArray = self.bankSectionArray[bankSection] {
                 let bank = bankArray[section]
                return bank.isCollapsed ? 0 : bank.accounts.count
            }
        }
        return 0
    }
    
    func didSelectRow(bankSection: BankSection, indexPath: IndexPath) {
        if let bankArray = self.bankSectionArray[bankSection] {
            let bank = bankArray[indexPath.section]
            selectedAccount = bank.accounts[indexPath.row]
            view.showAccount()
        }
    }
    
    func prepare(segue: UIStoryboardSegue) {
        if let accountVC = segue.destination as? AccountViewController  {
            let presenter = AccountPresenter(view: accountVC)
            presenter.account = selectedAccount
            accountVC.presenter = presenter
        }
    }
}

extension MyAccountsPresenter: BankHeaderPresenterProtocol {
    func didTapHeader(bankSection: BankSection, section: Int) {
        
        if let bankArray = self.bankSectionArray[bankSection] {
            let bank = bankArray[section]
            bank.isCollapsed = !bank.isCollapsed
        }
        
        view.reloadSection(bankSection: bankSection, section: section)
    }
}
