//
//  MyAccountsProtocol.swift
//  Banque-CA
//
//  Created by Gabriel on 22/12/2019.
//  Copyright © 2019 Gabriel. All rights reserved.
//

import UIKit

protocol MyAccountsViewControllerProtocol {
    func reloadData()
    func reloadSection(bankSection: BankSection, section: Int)
    func showAccount()
}

protocol MyAccountsPresenterProtocol {
    func load()
    func prepare(segue: UIStoryboardSegue)
    
    func getBankHeader(bankSection: BankSection, section: Int, cell: BankHeaderCell) -> BankHeaderCell
    func getBankAccountCell(bankSection: BankSection, indexPath: IndexPath, cell: BankAccountCell) -> BankAccountCell
    func getSectionBankAccountCount(bankSection: BankSection) -> Int
    func getBankAccountCount(bankSection: BankSection, section: Int) -> Int
    func didSelectRow(bankSection: BankSection, indexPath: IndexPath)
}
