//
//  AccountPresenter.swift
//  Banque-CA
//
//  Created by Gabriel on 23/12/2019.
//  Copyright © 2019 Gabriel. All rights reserved.
//

import Foundation

class AccountPresenter {

    let view: AccountViewControllerProtocol
    
    var account: AccountData?
    
    init(view: AccountViewControllerProtocol) {
        self.view = view
    }
}

extension AccountPresenter: AccountPresenterProtocol {
    
    func load() {
        if let account = account {
            view.setAccountInfos(title: account.title, amount: account.amount)
        }
        
        account?.operations.sort { $0.date > $1.date }
        
        account?.operations.sort(by: { (operationA, operationB) -> Bool in
            if operationA.getDate() == operationB.getDate() {
                return operationA.title < operationB.title
            }
            return operationA.getDate() > operationB.getDate()
        })
        
        view.reloadData()
    }
    
    func getOperationsCount() -> Int {
        return account?.operations.count ?? 0
    }
    
    func getOperationsCell(at indexPath: IndexPath, cell: BankOperationCell) -> BankOperationCell {
        
        if let operation = account?.operations[indexPath.row] {
            cell.titleLabel.text = operation.title
            cell.amountLabel.text = "\(operation.amount) €"
            cell.dateLabel.text = operation.getDate()
        }
        
        return cell
    }
}
