//
//  AccountProtocol.swift
//  Banque-CA
//
//  Created by Gabriel on 23/12/2019.
//  Copyright © 2019 Gabriel. All rights reserved.
//

import Foundation

protocol AccountViewControllerProtocol {
    func reloadData()
    func setAccountInfos(title: String, amount: String)
}

protocol AccountPresenterProtocol {
    func load()
    
    func getOperationsCount() -> Int
    func getOperationsCell(at indexPath: IndexPath, cell: BankOperationCell) -> BankOperationCell
}
