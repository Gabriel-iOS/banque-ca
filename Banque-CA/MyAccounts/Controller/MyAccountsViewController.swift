//
//  MyAccountsViewController.swift
//  Banque-CA
//
//  Created by Gabriel on 22/12/2019.
//  Copyright © 2019 Gabriel. All rights reserved.
//

import Foundation
import UIKit

class MyAccountsViewController: UIViewController {
    
    var presenter: MyAccountsPresenterProtocol?
    
    @IBOutlet weak var bankCreditAgricoleTableView: UITableView!
    @IBOutlet weak var bankCreditAgricoleTableViewConstraintHeight: NSLayoutConstraint!
    
    @IBOutlet weak var bankOthersTableView: UITableView!
    
    let showAccountSegue = "showAccount"
    
    override func viewDidLoad() {
        navigationController?.navigationBar.prefersLargeTitles = true
        title = "Mes comptes"
        
        initializeTableViews()
        
        presenter = MyAccountsPresenter(view: self, manager: MyAccountManager())
        
        presenter?.load()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        presenter?.prepare(segue: segue)
    }
}

extension MyAccountsViewController {
    
    private func initializeTableViews() {
        bankCreditAgricoleTableView.delegate = self
        bankCreditAgricoleTableView.dataSource = self
        bankCreditAgricoleTableView.register(BankHeaderCell.nib, forHeaderFooterViewReuseIdentifier: BankHeaderCell.identifier)
        bankCreditAgricoleTableView.register(BankAccountCell.nib, forCellReuseIdentifier: BankAccountCell.identifier)
        
        bankOthersTableView.delegate = self
        bankOthersTableView.dataSource = self
        bankOthersTableView.register(BankHeaderCell.nib, forHeaderFooterViewReuseIdentifier: BankHeaderCell.identifier)
        bankOthersTableView.register(BankAccountCell.nib, forCellReuseIdentifier: BankAccountCell.identifier)
    }
    
    private func getBankSection(with tableView: UITableView) -> BankSection {
        switch tableView {
        case bankCreditAgricoleTableView:
            return BankSection.creditAgricole
        default:
            return BankSection.other
        }
    }
    
    private func getBankTableView(with bankSection: BankSection) -> UITableView {
        switch bankSection {
        case .creditAgricole:
            return bankCreditAgricoleTableView
        default:
            return bankOthersTableView
        }
    }
    
    private func resizeBankCreditAgricoleTableView() {
        let height = bankCreditAgricoleTableView.contentSize.height
        bankCreditAgricoleTableViewConstraintHeight.constant = height
    }
}

extension MyAccountsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        let bankSection = getBankSection(with: tableView)
        return presenter?.getSectionBankAccountCount(bankSection: bankSection) ?? 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let headerCell = tableView.dequeueReusableHeaderFooterView(withIdentifier: BankHeaderCell.identifier) as? BankHeaderCell {
            let bankSection = getBankSection(with: tableView)
            return presenter?.getBankHeader(bankSection: bankSection, section: section, cell: headerCell) ?? UIView()
        }
        
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let bankSection = getBankSection(with: tableView)
        return presenter?.getBankAccountCount(bankSection: bankSection, section: section) ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: BankAccountCell.identifier) as? BankAccountCell {
            let bankSection = getBankSection(with: tableView)
            return presenter?.getBankAccountCell(bankSection: bankSection, indexPath: indexPath, cell: cell) ?? UITableViewCell()
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let bankSection = getBankSection(with: tableView)
        presenter?.didSelectRow(bankSection: bankSection, indexPath: indexPath)
    }
}

extension MyAccountsViewController: MyAccountsViewControllerProtocol {
    
    func reloadData() {
        bankCreditAgricoleTableView.reloadData()
        bankOthersTableView.reloadData()
        resizeBankCreditAgricoleTableView()
    }
    
    func reloadSection(bankSection: BankSection, section: Int) {
        
        let tableView = getBankTableView(with: bankSection)
        tableView.beginUpdates()
        tableView.reloadSections([section], with: .fade)
        tableView.endUpdates()
        
        if tableView == bankCreditAgricoleTableView {
            resizeBankCreditAgricoleTableView()
        }
    }
    
    func showAccount() {
        performSegue(withIdentifier: showAccountSegue, sender: self)
    }
}
