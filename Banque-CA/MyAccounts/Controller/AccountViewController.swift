//
//  AccountViewController.swift
//  Banque-CA
//
//  Created by Gabriel on 23/12/2019.
//  Copyright © 2019 Gabriel. All rights reserved.
//

import Foundation
import UIKit

class AccountViewController: UIViewController {
    
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var operationTableView: UITableView!
    
    var presenter: AccountPresenterProtocol?
    
    override func viewDidLoad() {
        
        initializeTableView()
        
        presenter?.load()
    }
}


extension AccountViewController {
    private func initializeTableView() {
        operationTableView.delegate = self
        operationTableView.dataSource = self
    }
}

extension AccountViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: BankOperationCell.identifier) as? BankOperationCell {
            return presenter?.getOperationsCell(at: indexPath, cell: cell) ?? UITableViewCell()
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter?.getOperationsCount() ?? 0
    }
    
}

extension AccountViewController: AccountViewControllerProtocol {
    func reloadData() {
        operationTableView.reloadData()
    }
    
    func setAccountInfos(title: String, amount: String) {
        titleLabel.text = title
        amountLabel.text = "\(amount) €"
    }
}
