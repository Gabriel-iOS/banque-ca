//
//  BankHeaderCell.swift
//  Banque-CA
//
//  Created by Gabriel on 23/12/2019.
//  Copyright © 2019 Gabriel. All rights reserved.
//

import UIKit

class BankHeaderCell: UITableViewHeaderFooterView {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    
    @IBOutlet weak var arrowImage: UIImageView!
    
    static let identifier = "BankHeaderCell"
    var section: Int = 0
    var bankSection: BankSection = .creditAgricole
    var isCollapsed = true
    var delegate: BankHeaderPresenterProtocol?
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapHeader)))
    }
    
    func setCollapsed(collapsed: Bool) {
        arrowImage.rotate(collapsed ? 0.0 : .pi)
    }
    
    @objc private func didTapHeader() {
        isCollapsed = !isCollapsed
        delegate?.didTapHeader(bankSection: bankSection, section: section)
    }
}
