//
//  BankHeaderProtocol.swift
//  Banque-CA
//
//  Created by Gabriel on 23/12/2019.
//  Copyright © 2019 Gabriel. All rights reserved.
//

import Foundation

protocol BankHeaderPresenterProtocol {
    func didTapHeader(bankSection: BankSection, section: Int)
}
