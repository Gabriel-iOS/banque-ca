//
//  MyAccountManager.swift
//  Banque-CA
//
//  Created by Gabriel on 22/12/2019.
//  Copyright © 2019 Gabriel. All rights reserved.
//

import Foundation

class MyAccountManager {
    
    private let urlGetAllBankAccount = "http://demo0576531.mockable.io/accounts"
    
    func getAllBankAccount(completion: @escaping (Array<BankData>?, Error?) -> Void) {
        
        Manager.shared.sendRequest(urlGetAllBankAccount, parameters: [:]) { (value, error) in
            
            guard let value = value else {
                print("Error while fetching tags: \(String(describing: error))")
                completion(nil, error)
                return
            }
            
            do {
                let newJSONDecoder = JSONDecoder()
                let itemList = try newJSONDecoder.decode(Array<BankData>.self, from: value)
                //self.launchList = itemList
                completion(itemList, nil)
                
            } catch {
                print(error)
            }
        }
    }
}
