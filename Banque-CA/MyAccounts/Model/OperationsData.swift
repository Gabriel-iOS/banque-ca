//
//  OperationsData.swift
//  Banque-CA
//
//  Created by Gabriel on 22/12/2019.
//  Copyright © 2019 Gabriel. All rights reserved.
//

import Foundation

class OperationData: Decodable {
    
    let id: String
    let title: String
    let amount: String
    let date: String
    
    init(id: String, title: String, amount: String, date: String) {
        self.id = id
        self.title = title
        self.amount = amount
        self.date = date
    }
    
    func getDate() -> String {
        guard let date = TimeInterval(self.date) else {
            return ""
        }
        let dateConverted = Date(timeIntervalSinceReferenceDate: date)
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "dd/MM/yyyy"
        return dateFormat.string(from: dateConverted)
    }        
}
