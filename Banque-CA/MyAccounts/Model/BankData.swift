//
//  BankData.swift
//  Banque-CA
//
//  Created by Gabriel on 22/12/2019.
//  Copyright © 2019 Gabriel. All rights reserved.
//

import Foundation

class BankData: Decodable {
    
    let id: String
    let title: String
    let isCA: Int
    var amount: String
    var accounts: [AccountData]
    
    var isCollapsed = true
    
    init(id: String, title: String, isCA: Int, amount: String, accounts: [AccountData]) {
        self.id = id
        self.title = title
        self.isCA = isCA
        self.amount = amount
        self.accounts = accounts
    }
    
    private enum CodingKeys: String, CodingKey {
        case id
        case title
        case isCA
        case amount
        case accounts
    }

}
