//
//  BankListData.swift
//  Banque-CA
//
//  Created by Gabriel on 22/12/2019.
//  Copyright © 2019 Gabriel. All rights reserved.
//

import Foundation

class BankListData: Decodable {
    var bankArray: Dictionary<String, BankData>
    
    init(bankArray: Dictionary<String, BankData>) {
        self.bankArray = bankArray
    }
}
