//
//  AccountData.swift
//  Banque-CA
//
//  Created by Gabriel on 22/12/2019.
//  Copyright © 2019 Gabriel. All rights reserved.
//

import Foundation

class AccountData: Decodable {
    
    let id: String
    let title: String    
    var amount: String
    var operations: [OperationData]
    
    init(id: String, title: String, amount: String, operations: [OperationData]) {
        self.id = id
        self.title = title
        self.amount = amount
        self.operations = operations
    }
}
