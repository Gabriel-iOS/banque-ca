//
//  SimulationProtocol.swift
//  Banque-CA
//
//  Created by Gabriel on 23/12/2019.
//  Copyright © 2019 Gabriel. All rights reserved.
//

protocol SimulationViewControllerProtocol {
    func setResultat(text: String)
}

protocol SimulationPresenterProtocol {    
    func calculate(amount: Double, taux: Double, time: Int)
}
