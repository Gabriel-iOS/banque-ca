//
//  SimulationViewController.swift
//  Banque-CA
//
//  Created by Gabriel on 23/12/2019.
//  Copyright © 2019 Gabriel. All rights reserved.
//

import UIKit

class SimulationViewController: UIViewController {
    
    @IBOutlet weak var tauxInput: UITextField!
    @IBOutlet weak var amountInput: UITextField!
    @IBOutlet weak var timeInput: UITextField!
    @IBOutlet weak var resultInput: UITextField!
    
    var presenter: SimulationPresenterProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        title = "Simulation de crédit"
        
        presenter = SimulationPresenter(view: self)
    }
    
    @IBAction func didTapCalculateButton(_ sender: Any) {
        
        guard let amountText = amountInput.text,
            let tauxText = tauxInput.text,
            let timeText = timeInput.text,
            let amount = Double(amountText),
            let taux = Double(tauxText),
            let time = Int(timeText)
        else {
            return
        }
        
        view.endEditing(true)
        presenter?.calculate(amount: amount, taux: taux, time: time)
    }
}

extension SimulationViewController: SimulationViewControllerProtocol {
    func setResultat(text: String) {
        resultInput.text = "Mensualités HT: \(text)"
    }
}
