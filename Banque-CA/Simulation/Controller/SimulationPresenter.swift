//
//  SimulationPresenter.swift
//  Banque-CA
//
//  Created by Gabriel on 23/12/2019.
//  Copyright © 2019 Gabriel. All rights reserved.
//

import Foundation
import UIKit

class SimulationPresenter {
    
    let view: SimulationViewControllerProtocol
    
    init(view: SimulationViewControllerProtocol) {
        self.view = view
    }
}

extension SimulationPresenter: SimulationPresenterProtocol {
    func calculate(amount: Double, taux: Double, time: Int) {
    
        let tauxYear = (taux / 100) / 12
        let amountYear = amount * tauxYear
        
        let monthTime = Double(time * 12)
        
        var timeCalculated = (1 + tauxYear)
        timeCalculated =  pow(timeCalculated, -monthTime)
        timeCalculated = 1 - timeCalculated
        
        let result = (amountYear / timeCalculated).rounded(toPlaces: 2)
        let resultText = "\(result) €"
        
        view.setResultat(text: resultText)
    }
}
