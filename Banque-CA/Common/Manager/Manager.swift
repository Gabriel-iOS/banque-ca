//
//  Manager.swift
//  Banque-CA
//
//  Created by Gabriel on 22/12/2019.
//  Copyright © 2019 Gabriel. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class Manager: NSObject {
    
    static let shared = Manager()
    
    
    var isConnectedToInternet:Bool {
        return NetworkReachabilityManager()!.isReachable
    }
    
    
    func checkInternetAndAlertIfError(){
        if self.isConnectedToInternet == false {
            AlertHandler.shared.showAlertConectivityProlbem()
        }
    }
    
    
    func sendRequest(_ url: String, parameters: [String: String], completion: @escaping (Data?, Error?) -> Void){
        
        Alamofire.request(url,
                          parameters: parameters)
            // 2
            .responseData { response in
                guard response.result.isSuccess,
                    let value = response.result.value else {
                        print("Error while fetching tags: \(String(describing: response.result.error))")
                        
                        self.checkInternetAndAlertIfError()
                        
                        completion(nil, response.result.error)
                        return
                }
                
                completion(value, nil)
        }
    }
    
    
    static func getStringValueFromJson(json : Any, key: String) -> String{
        return JSON(json)[key].stringValue
    }
    
}
