//
//  UIView+extensions.swift
//  Banque-CA
//
//  Created by Gabriel on 23/12/2019.
//  Copyright © 2019 Gabriel. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func rotate(_ toValue: CGFloat, duration: CFTimeInterval = 0.3) {
        let animation = CABasicAnimation(keyPath: "transform.rotation")
        animation.toValue = toValue
        animation.duration = duration
        animation.isRemovedOnCompletion = false
        animation.fillMode = CAMediaTimingFillMode.forwards
        self.layer.add(animation, forKey: nil)
    }
}
