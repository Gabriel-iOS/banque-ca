//
//  Double+extension.swift
//  Banque-CA
//
//  Created by Gabriel on 24/12/2019.
//  Copyright © 2019 Gabriel. All rights reserved.
//

import Foundation

extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
